import autoprefixer from "autoprefixer";
import browserSync from "browser-sync";
import cssnano from "cssnano";
import { dest, series, src, task, watch } from "gulp";
import sass from "gulp-sass";
import postcss from "gulp-postcss";
import rename from "gulp-rename";
import purgecss from "@fullhuman/postcss-purgecss";
import atimport from "postcss-import";
import tailwindcss from "tailwindcss";

const SITE_ROOT = "./_site";
const POST_BUILD_STYLESHEET = `${SITE_ROOT}/assets/css`;
const POST_RENAME_STYLESHEET = "style.css";
const PRE_BUILD_STYLESHEET = "./_sass/main.scss";
const TAILWIND_CONFIG = "./tailwind.config.js";

// Fix for Windows compatibility
const isWindowsPlatform = process.platform === "win32";
const jekyll = isWindowsPlatform ? "jekyll.bat" : "jekyll";
const spawn = isWindowsPlatform
  ? require("win-spawn")
  : require("child_process").spawn;

const isDevelopmentBuild = process.env.NODE_ENV === "development";

task("build-jekyll", () => {
  browserSync.notify("Building Jekyll site...");

  const args = ["exec", jekyll, "build"];

  if (isDevelopmentBuild) {
    args.push("--incremental");
    args.push("--trace");
  }

  return spawn("bundle", args, { stdio: "inherit" });
});

task("process-styles", () => {
  browserSync.notify("Compiling styles...");

  return src(PRE_BUILD_STYLESHEET)
    .pipe(sass())
    .pipe(
      postcss([
        atimport(),
        tailwindcss(TAILWIND_CONFIG),
        ...(!isDevelopmentBuild
          ? [
              purgecss({
                content: [`${SITE_ROOT}/**/*.html`],
                defaultExtractor: content =>
                  content.match(/[\w-/:]+(?<!:)/g) || []
              }),
              autoprefixer(),
              cssnano()
            ]
          : [])
      ])
    )
    .pipe(rename(POST_RENAME_STYLESHEET))
    .pipe(dest(POST_BUILD_STYLESHEET));
});

task("start-server", () => {
  browserSync.init({
    files: [SITE_ROOT + "/**"],
    open: "local",
    port: 4000,
    server: {
      baseDir: SITE_ROOT,
      serveStaticOptions: {
        extensions: ["html"]
      }
    }
  });

  watch(
    [
      "**/*.css",
      "**/*.scss",
      "**/*.html",
      "**/*.js",
      "**/*.md",
      "**/*.markdown",
      "!_site/**/*",
      "!node_modules/**/*"
    ],
    { interval: 500 },
    buildSite
  );
});

const buildSite = series("build-jekyll", "process-styles");

exports.serve = series(buildSite, "start-server");
exports.default = series(buildSite);