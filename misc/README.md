# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/misc/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/misc/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="misc/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="misc/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="misc/favicon-16x16.png">
    <link rel="manifest" href="misc/site.webmanifest">
    <link rel="mask-icon" href="misc/safari-pinned-tab.svg" color="#48bb78">
    <link rel="shortcut icon" href="misc/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Wahay Conference Calls">
    <meta name="application-name" content="Wahay Conference Calls">
    <meta name="msapplication-TileColor" content="#00a300">
    <meta name="msapplication-TileImage" content="misc/mstile-144x144.png">
    <meta name="msapplication-config" content="misc/browserconfig.xml">
    <meta name="theme-color" content="#48bb78">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)