![alt text](logo.png "Alasxpress")

# [Alasxpress](https://www.alasxpress.com/)

> Official website of Alas, a company that offers service of storage, orders preparation and delivering of packages with coverage to more than 320 regions in Chile.

------

This is the repository of Alasxpress website. The current online version of this portal can be found at [https://www.alasxpress.com](https://www.alasxpress.com/).

This website has been developed using [Jekyll](https://jekyllrb.com/).

## Jekyll

[Jekyll](https://jekyllrb.com/) is a simple, blog-aware, static site generator for personal, project, or organization sites.

Its code can be found at [GitHub](https://github.com/jekyll/jekyll).

## Compiling

To compile a local version of the website do:

1. Clone this repository:

```
$ git clone git@gitlab.com:alas2/alasxpress.git
```

2. Install Jekyll and its needed dependencies:

```
$ cd alasxpress
$ bundle config set deployment 'true'
$ bundle config set path 'vendor/bundle'
&& bundle install
```

3. Install Yarn and Node.js dependencies (Debian/Ubuntu)

```
$ curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
$ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
$ sudo apt update && sudo apt install yarn
```

Then, in the Alasxpress website folder run:

```
$ yarn
```

More information about Yarn installation can be found in the following [link](https://yarnpkg.com/getting-started/install).

3. Building the website:

```
# development
$ yarn build:dev

# production
$ yarn build:production # or simple `yarn build`
```

For running a local version of the website:

```
$ yarn start
```

Browse to address pointed by Yarn command line.
