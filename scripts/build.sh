#!/usr/bin/env bash

yarn install
yarn build:production
mkdir -p wahay_website_build
cp -r downloads/ wahay_website_build/
cp -r _site/* wahay_website_build/